package com.product.test.server.udp;

import com.product.test.server.Data;
import com.product.test.server.LoggerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.reactivex.Observer;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DatagramChannelInitializer extends ChannelInitializer<NioDatagramChannel> {

    private Observer<? super Data> observer;

    @Override
    protected void initChannel(NioDatagramChannel ch) {
        ch.pipeline()
//            .addLast(new LoggerHandler())
            .addLast(new PacketHandler(observer));
    }
}

package com.product.test.server.udp;

import com.product.test.server.Data;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.reactivex.Observer;
import lombok.AllArgsConstructor;

import static com.product.test.Launcher.j;

@AllArgsConstructor
public class PacketHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    private Observer<? super Data> observer;

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, DatagramPacket packet) {
        j.addAndGet(1);
        ByteBuf content = packet.content();

        try {
            byte[] buf = new byte[content.readableBytes()];
            content.readBytes(buf, 0, content.readableBytes());
            observer.onNext(new Data(packet.sender(), buf));
        }
        finally {
            content.retain();
            content.release();
            channelHandlerContext.flush();
        }
    }
}

package com.product.test.server.udp;

import com.product.test.server.Data;
import com.product.test.server.Server;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.reactivex.Observer;

public class UdpServer extends Server<Data> {
    private NioEventLoopGroup workers;

    public UdpServer(int port) {
        super(port);
        workers = new NioEventLoopGroup();
    }

    public UdpServer(int port, int numOfThreads) {
        super(port);
        workers = new NioEventLoopGroup(numOfThreads);
    }

    protected Bootstrap setupServer(Observer<? super Data> observer) {
        Bootstrap serverBootstrap = new Bootstrap();

        serverBootstrap
            .group(workers)
            .channel(NioDatagramChannel.class)
            .handler(new DatagramChannelInitializer(observer));

        return serverBootstrap;
    }
}

package com.product.test.server;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.net.SocketAddress;

@AllArgsConstructor
@Getter
@ToString
public class Data {
    private SocketAddress source;
    private byte[] bytes;
}

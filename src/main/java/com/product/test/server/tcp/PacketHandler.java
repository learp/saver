package com.product.test.server.tcp;

import com.product.test.server.Data;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import io.reactivex.Observer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

@AllArgsConstructor
@Slf4j
class PacketHandler extends ChannelInboundHandlerAdapter {

    private Observer<? super Data> observer;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf byteBuf = (ByteBuf) msg;

        byte[] buf = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(buf, 0, byteBuf.readableBytes());
//        System.out.println(Thread.currentThread().getName());
        byteBuf.release();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER)
            .addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("Exception: {}. Stack trace: {}", cause, getStackTrace(cause));
        ctx.close();
    }

    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf packet) {
        try {
            byte[] buf = new byte[packet.readableBytes()];
            packet.readBytes(buf, 0, packet.readableBytes());
            System.out.println(Thread.currentThread().getName());
            observer.onNext(new Data(channelHandlerContext.channel().remoteAddress(), buf));
        }
        finally {
            packet.release();
            channelHandlerContext.flush();
        }
    }
}

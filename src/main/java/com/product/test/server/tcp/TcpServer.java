package com.product.test.server.tcp;

import com.product.test.server.Data;
import com.product.test.server.Server;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.reactivex.Observer;

public class TcpServer extends Server<Data> {
    private NioEventLoopGroup bosses;
    private NioEventLoopGroup workers;

    public TcpServer(int port) {
        super(port);
        bosses = new NioEventLoopGroup();
        workers = new NioEventLoopGroup();
    }

    public TcpServer(int port, int numOfThreads) {
        super(port);
        bosses = new NioEventLoopGroup();
        workers = new NioEventLoopGroup(numOfThreads);
    }

    protected ServerBootstrap setupServer(Observer<? super Data> observer) {
        ServerBootstrap serverBootstrap = new ServerBootstrap();

        serverBootstrap
            .group(bosses, workers)
            .channel(NioServerSocketChannel.class)
            .option(ChannelOption.SO_BACKLOG, 10000)
            .childOption(ChannelOption.SO_KEEPALIVE, false)
//            .childOption(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 32 * 1024)
//            .childOption(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 8 * 1024)
            .childHandler(new SocketChannelInitializer(observer));

        return serverBootstrap;
    }
}

package com.product.test.server.tcp;

import com.product.test.server.Data;
import com.product.test.server.LoggerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.reactivex.Observer;
import lombok.AllArgsConstructor;

@AllArgsConstructor
class SocketChannelInitializer extends ChannelInitializer<SocketChannel> {

    private Observer<? super Data> observer;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline()
//            .addLast(new LoggerHandler())
            .addLast(new PacketHandler(observer));
    }
}

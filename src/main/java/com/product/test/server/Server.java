package com.product.test.server;

import io.netty.bootstrap.AbstractBootstrap;
import io.reactivex.Observable;
import io.reactivex.Observer;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class Server<T> extends Observable<T> {
    private int port;

    @Override
    protected void subscribeActual(Observer<? super T> observer) {
        AbstractBootstrap serverBootstrap = setupServer(observer);
        serverBootstrap.bind(getPort());
    }

    protected abstract AbstractBootstrap setupServer(Observer<? super T> observer);
}

package com.product.test.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ClientId {
    private SocketAddress socketAddress;
}

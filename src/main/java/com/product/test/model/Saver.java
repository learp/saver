package com.product.test.model;

import com.product.test.server.Data;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.product.test.Launcher.i;
import static com.product.test.Launcher.j;

public class Saver implements Observer<Data> {

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Data data) {
        // there is must be code that save data to file
        String x = new String(data.getBytes());
//        System.out.println(x);
//        System.out.println("Received! " + Thread.currentThread().getName());
        i.addAndGet(1);

        System.out.println(x);
        if (x.startsWith("hi")) {
            System.out.println(i);
            System.out.println(j);
        }
    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }
}

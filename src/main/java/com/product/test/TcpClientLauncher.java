package com.product.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;

import com.product.test.client.tcp.Client;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;


public class TcpClientLauncher {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(120);
        for (int i = 0; i < 100; i++) {
            executorService.submit(TcpClientLauncher::task);
        }
        executorService.shutdown();
        executorService.awaitTermination(300, TimeUnit.SECONDS);
    }

    private static void task() {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(10000));
            // Create a client
            System.out.println("Creating new TCP Client");

            Client client = new Client("185.100.106.192", 7684, 12);
            ChannelFuture channelFuture = client.startup();

            System.out.println("New Client is created");
            channelFuture.sync();
            // check the connection is successful
            if (channelFuture.isSuccess()) {
                String h = StringUtils.repeat('h', 1_000);
                byte[] bytes = h.getBytes();
                AtomicInteger all = new AtomicInteger();
                AtomicInteger ai = new AtomicInteger();
                for (int i = 0; i < 10_000_000; i++) {
                    if (i % 100_000 == 0) {
                        System.out.println(ai.get());
                    }
                    ChannelFuture await = channelFuture.channel().writeAndFlush(Unpooled.wrappedBuffer(bytes)).await();
                    if (!await.isSuccess()) {
                        throw new RuntimeException();
                    } else {
                        ai.incrementAndGet();
                    }
                }
                // send message to server
            }
            // timeout before closing client
            Thread.sleep(5000);
            // close the client
            client.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Try Starting Server First !!");
        } finally {

        }
    }
}
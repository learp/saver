package com.product.test;

import com.product.test.model.ClientId;
import com.product.test.model.Saver;
import com.product.test.server.Data;
import com.product.test.server.tcp.TcpServer;
import com.product.test.server.udp.UdpServer;
import io.reactivex.Observable;
import io.reactivex.observables.GroupedObservable;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.atomic.AtomicInteger;

public class Launcher {
    public static AtomicInteger i = new AtomicInteger();
    public static AtomicInteger j = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        Observable<? extends Data> udpServer = new UdpServer(39735, 12);
        Observable<? extends GroupedObservable<ClientId, ? extends Data>> groupedBySourceAddress =
            udpServer.groupBy(data -> new ClientId(data.getSource()));

        groupedBySourceAddress.subscribe(observable -> {
            observable.observeOn(Schedulers.computation()).subscribe(new Saver());
        });
    }
}
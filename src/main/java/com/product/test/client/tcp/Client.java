package com.product.test.client.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class Client {
    String host;
    int port;
    Channel channel;
    EventLoopGroup workGroup;

    /**
     * Constructor
     *
     * @param port {@link Integer} port of server
     */
    public Client(String host, int port, int numOfThreads) {
        this.port = port;
        this.host = host;
        this.workGroup = new NioEventLoopGroup(numOfThreads);
    }

    /**
     * Startup the client
     *
     * @return {@link ChannelFuture}
     * @throws Exception
     */
    public ChannelFuture startup() throws Exception {
        Bootstrap b = new Bootstrap();
        b.group(workGroup);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);
        b.handler(new ChannelInitializer<SocketChannel>() {
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                socketChannel.pipeline().addLast(new NettyHandler());
            }
        });
        ChannelFuture channelFuture = b.connect(host, port).sync();
        this.channel = channelFuture.channel();

        return channelFuture;
    }

    /**
     * Shutdown a client
     */
    public void shutdown() {
        workGroup.shutdownGracefully();
    }
}
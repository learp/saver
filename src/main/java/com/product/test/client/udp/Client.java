package com.product.test.client.udp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;

public class Client {
    String host;
    int port;
    Channel channel;
    EventLoopGroup workGroup;

    /**
     * Constructor
     *
     * @param port {@link Integer} port of server
     */
    public Client(String host, int port, int numOfThreads) {
        this.host = host;
        this.port = port;
        this.workGroup = new NioEventLoopGroup(numOfThreads);
    }

    /**
     * Startup the client
     *
     * @return {@link ChannelFuture}
     * @throws Exception
     */
    public ChannelFuture startup() throws Exception {
        try {
            Bootstrap b = new Bootstrap();
            b.group(workGroup);
            b.channel(NioDatagramChannel.class);
            b.handler(new ChannelInitializer<DatagramChannel>() {
                protected void initChannel(DatagramChannel datagramChannel) throws Exception {
                    datagramChannel.pipeline().addLast(new NettyHandler());
                }
            });
            ChannelFuture channelFuture = b.connect(host, this.port).sync();
            this.channel = channelFuture.channel();

            return channelFuture;
        } finally {
        }
    }

    /**
     * Shutdown a client
     */
    public void shutdown() {
        workGroup.shutdownGracefully();
    }
}